<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\file\FileInput;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $model backend\models\Pages */

$this->title = $model->name;
?>
<div class="pages-view">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
      //  $model->content = strip_tags($model->content),
        'attributes' => [
            'name',
            'content:ntext',
        ],
    ]) ?>
</div>

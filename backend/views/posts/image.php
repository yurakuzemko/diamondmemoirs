<?php
/**
 * Created by PhpStorm.
 * User: Yurii
 * Date: 1/3/2016
 * Time: 11:08 PM
 */
use yii\helpers\Html;
$this->title = 'Images';
?>
<?php

$files = array_reverse($files);
$files = array_diff($files, array('.', '..'));
echo "<div class='mainimage'>";
foreach ($files as $file): ?>
<div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
        <img src="/uploads/1/<?=$file?>" >
    </a>
    <a class="next" href="index.php?r=site%2Fimage"></a>
</div>
<?php endforeach; ?>
</div>

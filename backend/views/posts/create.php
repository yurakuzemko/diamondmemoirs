<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model backend\models\Posts */

$this->title = 'Create Post';
?>
<div class="posts-create">
 <?php   Modal::begin([
    'header'=>'Select main image',
    'id'=>'modal',
    'size'=>'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
 ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

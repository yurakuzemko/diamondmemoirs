<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PostsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posts';
?>
<div class="posts-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'category.name'=>[
                'label'=>'Category',
                'attribute'=>'category.name'
            ],
           // 'category_id',
            'title',
           // 'image',
            'text:ntext'=>[
                'attribute' => 'text',
                'format' => 'raw',
                'value' => function($data){
                    return strip_tags(substr($data->text, 0, 255));
                }
            ],
            'create_date'=>[
                'attribute' => 'create_date',
                'format' => 'raw',
                'value' => function($data){
                    return date('Y\.n\.j',$data->create_date);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
            'buttons'=>[
                'view' => function ($url,$model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>',
                        \yii\helpers\Url::to('@front').'/site/read?id='.$model->id,
                        ['target'=>'blank']
                        );
                },
            ],
          ],
        ],
    ]);

    ?>

</div>
<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Admin Panel';
?>
<div class="jumbotron">
    <h1>Welcome!</h1>
    <p>diamond memoirs admin panel</p>
    <p><?= Html::a('Manage Posts', ['posts/index'], ['class' => 'btn btn-primary btn-lg']) ?></p>
</div>

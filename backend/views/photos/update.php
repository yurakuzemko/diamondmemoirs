<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Photos */

$this->title = 'Edit Photo: ' . ' ' . $model->title;
?>
<div class="photos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

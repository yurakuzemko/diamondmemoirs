<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Photos */

$this->title = 'Add Photo';
?>
<div class="photos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Categories */

$this->title = 'Add Category';
?>
<div class="categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

/**
 * Created by Yurii on 1/4/2016.
 */
$(function(){
   $('#modalButton').click(function(){
       $('#modal').modal('show')
           .find('#modalContent')
           .load($(this).attr('value'));
       $(document).on('click', "a.thumbnail", function() {
           $('input#posts-image').val($($(this).children("img")).attr('src'));
           $('#modal').modal('hide');
       });
   });
});
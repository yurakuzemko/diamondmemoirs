<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
	 'urlManager' => [
            // Disable index.php
            'showScriptName' => false,
            // Disable r= routes
            'enablePrettyUrl' => true,

        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],

    ],
    'modules' => [
        'redactor' => 'yii\redactor\RedactorModule',
        'class' => 'yii\redactor\RedactorModule',
        'uploadDir' => '@admin/uploads',
        'uploadUrl' => '/hello/uploads',
    ],
    'aliases'=>[
      'front'=>'http://diamond',
        'admin'=>'http://admin.diamond'
    ],
    'params' => $params,
];

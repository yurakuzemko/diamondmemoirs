/**
 * Created by Yurii on 1/1/2016.
 */
$(document).ready(function(){

    $('.summary').hide();
    $('#categories li').click(function(){
        $('div').remove('.message');
        if(!$('.category'+$(this).attr('id'))[0]){
            if($(this).attr('id')!='All') {
                $('.posts').append("<div class='message'><div class='alert alert-info' role='alert'><b>No posts yet</b></div></div>")
            }
            }
        if($(this).attr('id')!='All'){
            $('#categories li').removeClass('active');
            $(this).addClass('active');
            $('.blog-post').hide();
            $('.category'+$(this).attr('id')+' .blog-post').show();

        }else{
            $('#categories li').removeClass('active');
            $(this).addClass('active');
            $('.blog-post').show();
        }
    });
    $.each($('.gallery-item img'), function (){
        var realWith = $(this).width();
        var realHeight = $(this).height();
        var h = 150;
        var k = realWith/realHeight;
        var w = h*k;
        $(this).css({
            'height' : h,
            'width': w,
            'margin-left': Math.floor((Math.random() * 50) + 1)
        });

    });
    $('.gallery-item').css({
        'opacity': 100
    });

});

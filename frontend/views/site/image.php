<?php
use yii\widgets\LinkPager;
/**
 * Created by PhpStorm.
 * User: Yurii
 * Date: 1/7/2016
 * Time: 9:12 PM
 */
$this->title = 'Photos';
$data = $dataProvider->getModels();
if(empty($data)){
    echo "<div class=\"alert alert-info\" role=\"alert\"><h1>No photos yet</h1></div>";
} ?>
<div class="photos">

    <?php   $items = array();
foreach ($data as $file):
    array_push($items,
        [
            'url' => $file->path,
            'src' => $file->path,
            'options' => array('title' => $file->title)
        ]
    );
endforeach;
?>
<?= dosamigos\gallery\Gallery::widget(['items' => $items]);?>

</div>
<?= LinkPager::widget([
    'pagination' => $pages,
]); ?>

<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
?>
<div class="site-about">
    <?php foreach ($data as $post):?>
        <?php if($post->id==1){ ?>
    <h1><?= Html::encode($post->name) ?></h1>

        <p><?php echo $post->content; ?></p>

  <?php }?>
    <?php endforeach; ?>
</div>

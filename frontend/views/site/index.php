<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\LinkPager;
/* @var $this yii\web\View */

$this->title = 'diamond memoirs';
?>
<div class="clearfix"></div>
<ul class="nav nav-pills nav-stacked" id="categories">
    <li role="presentation"><h2>Categories</h2></li>
    <li role="presentation" id="All" class="active"><?= Html::a('All',$url='#'); ?></li>
    <?php foreach($head as $category): ?>
        <li role="presentation" id="<?=$category->id?>"><?= Html::a($category->name,$url='#'); ?></li>
    <?php endforeach; ?>
</ul>
<div class="posts">


<!--    --><?php

    $data = $dataProvider->getModels();
    if(empty($data)){
        echo "<div class=\"alert alert-info\" role=\"alert\"><h1>No posts yet</h1></div>";
    }
    foreach($data as $model):
    ?>

    <div class="category<?=$model->category_id?>">
        <div class="blog-post" >
            <h2 class="blog-post-title"><?= Html::a($model->title, array('site/read', 'id'=>$model->id)); ?></h2>
            <p class="blog-post-meta"><?= Html::encode(date('d F Y',$model->create_date)); ?></p>
            <br>
            <div class="intro">
                <?php
                $text = preg_replace( "/<img.*>/","", $model->text );
                if(strlen($text)<=255){
                    echo Html::decode($text);
                } else {
                    echo Html::decode(substr($text, 0, 255)) . "...";
                    echo Html::a('Read more',array('site/read', 'id'=>$model->id));
                }
                echo '</div>';
                if($model->image != "")
                    echo Html::img($model->image,['href'=>array('site/read', 'id'=>$model->id)])
                ?>

            </div>
        </div>

  <?php  endforeach;?>
        <?= LinkPager::widget([
            'pagination' => $pages,
        ]); ?>

    </div>


<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
?>
<div class="site-contact">

    <?php foreach ($data as $post):?>
        <?php if($post->id==2){ ?>
            <h1><?= Html::encode($post->name) ?></h1>

            <p><?php echo $post->content; ?></p>

        <?php }?>
    <?php endforeach; ?>
</div>

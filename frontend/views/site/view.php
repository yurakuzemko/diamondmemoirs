
<?php
use yii\helpers\Html;

?>
<div class="category<?=$model->category_id?>">
         <div class="blog-post" >
            <h2 class="blog-post-title"><?= Html::a($model->title, array('site/read', 'id'=>$model->id)); ?></h2>
<p class="blog-post-meta"><?= Html::encode(date('d F Y',$model->create_date)); ?></p>
<br>
<div class="intro">
    <?php
    $text = preg_replace( "/<img.*>/","", $model->text );
    if(strlen($text)<=255){
        echo Html::decode($text);
    } else {
        echo Html::decode(substr($text, 0, 255)) . "...";
        echo Html::a('Read more',array('site/read', 'id'=>$model->id));
    }
    echo '</div>';
    if($model->image != "")
        echo Html::img($model->image,['href'=>array('site/read', 'id'=>$model->id)])
    ?>

</div>
</div>
<?php
use yii\widgets\ListView;
/**
 * Created by PhpStorm.
 * User: Yurii
 * Date: 1/7/2016
 * Time: 11:41 PM
 */
$this->title = 'Videos';
$videos = $dataProvider->getModels();
if(empty($videos)){
    echo "<div class=\"alert alert-info\" role=\"alert\"><h1>No videos yet</h1></div>";
}
echo "<div class='videos' >";
foreach ($videos as $video): ?>
    <h2><?=$video->name?></h2>
    <?=$video->code?>
<?php endforeach; ?>
</div>

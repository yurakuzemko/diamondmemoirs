<?php
/**
 * Created by PhpStorm.
 * User: Yurii
 * Date: 12/30/2015
 * Time: 5:13 PM
 */
use yii\helpers\Html;
use yii\helpers\Url; ?>
<?php $this->title = $post->title ?>
<div class="blog-post" >
    <h2 class="blog-post-title"><?php echo $post->title; ?></h2>
   <p class="blog-post-meta"> <?php echo date('d F Y',$post->create_date); ?></p>
<div class="intro"><p><?php echo $post->text; ?></p></div>
</div>
<div class="share">
    <h4>Share this:</h4>
<span class='st_facebook_large' displayText='Facebook'></span>
<span class='st_twitter_large' displayText='Tweet'></span>
<span class='st_pinterest_large' displayText='Pinterest'></span>
<span class='st_vkontakte_large' displayText='Vkontakte'></span>
<span class='st_tumblr_large' displayText='Tumblr'></span>
<span class='st_email_large' displayText='Email'></span>
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "75339d0b-7b4f-46fb-848f-a58570f4dddb", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</div>